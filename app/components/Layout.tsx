import { FC } from 'react';
import { Footer } from './Footer';
import { Navbar } from './Navbar';

export const Layout: FC = (props) => {
	return (
		<div className="content">
			<Navbar />
			{props.children}
			<Footer />
		</div>
	);
};
