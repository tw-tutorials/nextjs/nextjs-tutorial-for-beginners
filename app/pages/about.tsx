import type { NextPage } from 'next';
import Head from 'next/head';

const About: NextPage = () => {
	return (
		<>
			<Head>
				<title>Ninja List | About</title>
				<meta name="keywords" content="ninjas" />
				<meta name="description" content="Ninja list" />
			</Head>
			<div>
				<h1>About</h1>
				<p>
					Lorem ipsum dolor sit amet consectetur adipisicing elit. Deleniti repellendus accusamus
					cumque officia sunt ipsam et corporis expedita culpa minus id corrupti ad facilis illo,
					amet at eum magni reprehenderit. Lorem ipsum dolor sit amet consectetur adipisicing elit.
					Iure doloribus omnis consequatur sed laboriosam culpa quidem ullam. Officia, sequi! Quam
					ratione perspiciatis est, quo harum maiores consequatur aspernatur porro maxime. Lorem
					ipsum dolor sit amet, consectetur adipisicing elit. Fuga quas nesciunt quaerat ullam quia
					deserunt, atque possimus. Vitae, natus enim voluptatum ex architecto quos consectetur
					laboriosam, error harum autem quisquam?
				</p>
				<p>
					Lorem ipsum dolor sit amet consectetur adipisicing elit. Deleniti repellendus accusamus
					cumque officia sunt ipsam et corporis expedita culpa minus id corrupti ad facilis illo,
					amet at eum magni reprehenderit. Lorem ipsum dolor sit amet consectetur adipisicing elit.
					Iure doloribus omnis consequatur sed laboriosam culpa quidem ullam. Officia, sequi! Quam
					ratione perspiciatis est, quo harum maiores consequatur aspernatur porro maxime. Lorem
					ipsum dolor sit amet, consectetur adipisicing elit. Fuga quas nesciunt quaerat ullam quia
					deserunt, atque possimus. Vitae, natus enim voluptatum ex architecto quos consectetur
					laboriosam, error harum autem quisquam?
				</p>
			</div>
		</>
	);
};

export default About;
