import { GetStaticPaths, GetStaticProps, NextPage } from 'next';
import Head from 'next/head';
import { ParsedUrlQuery } from 'querystring';
import Ninjas from '.';
import { Ninja } from '../../types/Ninja';

interface Params extends ParsedUrlQuery {
	id: string;
}
interface Props {
	ninja: Ninja;
}

export const getStaticPaths: GetStaticPaths<Params> = async () => {
	console.log('fetch all data');
	const res = await fetch(`https://jsonplaceholder.typicode.com/users`);
	const data: Ninja[] = await res.json();

	const paths = data.map((ninja) => {
		return {
			params: { id: ninja.id.toString() },
		};
	});

	return { paths, fallback: false };
};

export const getStaticProps: GetStaticProps<Props, Params> = async (context) => {
	const id = context.params!.id;
	const res = await fetch(`https://jsonplaceholder.typicode.com/users/${id}`);
	const data: Ninja = await res.json();

	return {
		props: { ninja: data },
	};
};

const NinjaDetails: NextPage<Props> = (props) => {
	const { ninja } = props;
	return (
		<>
			<Head>
				<title>Ninja List | {ninja.name}</title>
				<meta name="description" content={ninja.name} />
			</Head>
			<div>
				<h1>{ninja.name}</h1>
				<p>{ninja.email}</p>
				<p>{ninja.website}</p>
				<p>{ninja.address.city}</p>
			</div>
		</>
	);
};

export default NinjaDetails;
