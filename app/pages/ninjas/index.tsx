import { GetStaticProps, NextPage } from 'next';
import Head from 'next/head';
import Link from 'next/link';
import { Ninja } from '../../types/Ninja';

import styles from '../../styles/NInjas.module.scss';

interface Props {
	ninjas: Ninja[];
}

export const getStaticProps: GetStaticProps<Props> = async () => {
	const res = await fetch('https://jsonplaceholder.typicode.com/users');
	const data: Ninja[] = await res.json();

	return {
		props: {
			ninjas: data,
		},
	};
};

const Ninjas: NextPage<Props> = (props) => {
	return (
		<>
			<Head>
				<title>Ninja List | Ninjas</title>
				<meta name="keywords" content="ninjas" />
				<meta name="description" content="Ninja list" />
			</Head>
			<div>
				<h1>Ninjas</h1>
				{props.ninjas.map((ninja) => (
					<Link key={ninja.id} href={`/ninjas/${ninja.id}`}>
						<a className={styles.single}>
							<h3>{ninja.name}</h3>
						</a>
					</Link>
				))}
			</div>
		</>
	);
};

export default Ninjas;
